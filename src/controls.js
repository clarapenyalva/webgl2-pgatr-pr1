

//Camera movement variables
var drag = false, move = false;
var prevX = 0, prevY = 0, dX = 0, dY = 0, rotX = 0, rotY = 0,
    radius = 5,
    speed = 5, rotationSpd = .05, wheelSpd = .02;

var systemPos = glMatrix.vec3.fromValues(0, 0, 5)
    , forward = glMatrix.vec3.fromValues(0, 0, -1)
    , up = glMatrix.vec3.fromValues(0, 1, 0)
    //,right = glMatrix.vec3.fromValues(1, 0, 0)
    ;

function getCurrentForward()
{
    newForward = glMatrix.vec3.clone(forward);
    var cameraRotation = glMatrix.mat4.create();
    glMatrix.mat4.identity(cameraRotation);
    var cameraRotation = glMatrix.mat4.fromYRotation(cameraRotation, rotX);
    glMatrix.mat4.rotate(cameraRotation, cameraRotation, rotY, glMatrix.vec3.fromValues(1, 0, 0));
    glMatrix.vec3.transformMat4(newForward, newForward, cameraRotation);
    return newForward;
}

class ReferenceSystem 
{
    constructor(forward, up, right) 
    {
        this.forward = forward;
        this.up = up;
        this.right = right;
    }
}

function getCurrentCameraRefSys(refSys)
{
    var cameraRotation = glMatrix.mat4.create();
    glMatrix.mat4.identity(cameraRotation);
    var cameraRotation = glMatrix.mat4.fromYRotation(cameraRotation, rotX);
    glMatrix.mat4.rotate(cameraRotation, cameraRotation, rotY, glMatrix.vec3.fromValues(1, 0, 0));

    var outForward = glMatrix.vec3.clone(forward);
    glMatrix.vec3.transformMat4(outForward, outForward, cameraRotation);

    var outUp = glMatrix.vec3.clone(up);
    glMatrix.vec3.transformMat4(outUp, outUp, cameraRotation);

    var outRight = glMatrix.vec3.create();
    outRight = glMatrix.vec3.cross(outRight, outForward, outUp);

    refSys.forward = outForward;
    refSys.up = outUp;
    refSys.right = outRight;
}

function setViewMat(viewMat)
{
    var newForward;
    newForward = getCurrentForward();

    var cameraPos = glMatrix.vec3.create();
    cameraPos = glMatrix.vec3.scale(cameraPos, newForward, -radius);
    cameraPos = glMatrix.vec3.add(cameraPos, cameraPos, systemPos);

    viewMat = glMatrix.mat4.lookAt(
        viewMat,//out
        cameraPos, //Eye
        systemPos, //Center
        up); //Up
    // console.log(viewMat);
}

function mouseWheel(e)
{
    radius = radius - wheelSpd * e.wheelDelta;
    var min = 3;
    var max = 100;
    radius = Math.min(Math.max(radius, min), max);
    setViewMat(viewMat);
    e.preventDefault();
}

function mouseDown(e)
{
    e.preventDefault();
    if (e.button == 0) {
        drag = true;
        prevX = e.pageX;
        prevY = e.pageY;
    }
    else if (e.button == 2) {
        move = true;
        prevX = e.pageX;
        prevY = e.pageY;
    }
};

function mouseUp(e)
{
    if (e.button == 0) {
        drag = false;
    }
    else if (e.button == 2) {
        move = false;
    }
};

function mouseMove(e)
{
    if (drag) {
        dX = (e.pageX - prevX) * 2 * Math.PI / canvas.width,
            dY = (e.pageY - prevY) * 2 * Math.PI / canvas.height;
        if (dX == 0 && dY == 0) return;

        rotX -= dX;

        var max = (Math.PI / 2.0) - .1;
        var min = -max;
        var value = Math.abs(rotY - dY);

        if (value < max)
            rotY -= dY;
        else
            rotY = Math.min(Math.max(rotY, min), max);

        // console.log("dY: " + -dY);
        // console.log("RotY: " + rotY);
        // console.log("Islessthan: " + (value < max));
        // console.log("-----------------------------")

        prevX = e.pageX;
        prevY = e.pageY;

        setViewMat(viewMat);
        e.preventDefault();

    }
    else if (move) {
        dX = (e.pageX - prevX) * 2 * Math.PI / canvas.width;
        dY = (e.pageY - prevY) * 2 * Math.PI / canvas.height;

        prevX = e.pageX;
        prevY = e.pageY;

        //var newForward = getCurrentForward();

        // We need to move the center point in the plane XZ
        // var planeNormal = glMatrix.vec3.fromValues(0, 1, 0);

        // var forwardOntoNormal = glMatrix.vec3.scale(glMatrix.vec3.create(), planeNormal, glMatrix.vec3.dot(newForward, planeNormal));
        // var forwardProjectedPlane = glMatrix.vec3.subtract(glMatrix.vec3.create(), newForward, forwardOntoNormal);
        // forwardProjectedPlane = glMatrix.vec3.normalize(forwardProjectedPlane, forwardProjectedPlane);

        //var rightProjectedPlane = glMatrix.vec3.cross(glMatrix.vec3.create(), forwardProjectedPlane, planeNormal);

        // var forwardMovement = glMatrix.vec3.scale(glMatrix.vec3.create(), forwardOntoNormal, -speed * dY);
        // var rightMovement = glMatrix.vec3.scale(glMatrix.vec3.create(), rightProjectedPlane, -speed * dX);
        // glMatrix.vec3.add(systemPos, systemPos, glMatrix.vec3.add(rightMovement, rightMovement, forwardMovement));

        var refSys = new ReferenceSystem();
        getCurrentCameraRefSys(refSys);

        var upMovement = glMatrix.vec3.scale(glMatrix.vec3.create(), refSys.up, speed * dY);
        var rightMovement = glMatrix.vec3.scale(glMatrix.vec3.create(), refSys.right, -speed * dX);
        glMatrix.vec3.add(systemPos, systemPos, glMatrix.vec3.add(rightMovement, rightMovement, upMovement));

        setViewMat(viewMat);
        e.preventDefault();
    }

};

// function setViewMat(viewMat)
// {
//     var aVector = glMatrix.vec3.create();
//     glMatrix.mat4.lookAt(viewMat, systemPos, glMatrix.vec3.add(aVector, systemPos, forward), up);
// }

// function onKeyDown(e)
// {
//     switch (e.keyCode)
//     {
//         case 87: //W
//         case 119:
//             //console.log("w");
//             //debugger;
//             //console.log(glMatrix.vec3.scale(forward, speed));
//             var aVector = glMatrix.vec3.create();
//             glMatrix.vec3.scale(aVector, forward, speed);
//             glMatrix.vec3.add(systemPos, systemPos, aVector);
//             //debugger;
//             setViewMat(viewMat);
//             break;

//         case 83: //S
//         case 115:
//             //console.log("s");
//             var aVector = glMatrix.vec3.create();
//             glMatrix.vec3.scale(aVector, forward, -1 * speed);
//             glMatrix.vec3.add(systemPos, systemPos, aVector);
//             setViewMat(viewMat);

//             break;

//         case 65: //A
//         case 97:
//             //console.log("a");
//             var aVector = glMatrix.vec3.create();
//             var right;
//             right = getRightVector(right);
//             glMatrix.vec3.scale(aVector, right, -1 * speed);
//             glMatrix.vec3.add(systemPos, systemPos, aVector);
//             setViewMat(viewMat);

//             break;

//         case 68: //D
//         case 100:
//             //console.log("d");
//             var aVector = glMatrix.vec3.create();
//             var right;
//             right = getRightVector(right);
//             glMatrix.vec3.scale(aVector, right, speed);
//             glMatrix.vec3.add(systemPos, systemPos, aVector);
//             setViewMat(viewMat);
//             break;

//         case 81: //Q
//         case 113:
//             var rot = glMatrix.mat4.fromRotation(
//                 glMatrix.mat4.create(), rotationSpd, up);
//             forward = glMatrix.vec3.transformMat4(forward, forward, rot);
//             setViewMat(viewMat);
//             break;

//         case 69: //E
//         case 101:
//             var rot = glMatrix.mat4.fromRotation(
//                 glMatrix.mat4.create(), -rotationSpd, up);
//             forward = glMatrix.vec3.transformMat4(forward, forward, rot);
//             setViewMat(viewMat);
//             break;
//         default:
//     }
// }

function InitializeControls(canvas)
{
    //Add events
    canvas.addEventListener("mousedown", mouseDown, false);
    canvas.addEventListener("mouseup", mouseUp, false);
    canvas.addEventListener("mouseout", mouseUp, false);
    canvas.addEventListener("mousemove", mouseMove, false);
    canvas.addEventListener("mousewheel", mouseWheel, false);
    // window.addEventListener("keypress", onKeyDown, false);
}

function getRightVector(out)
{
    out = glMatrix.vec3.create();
    out = glMatrix.vec3.cross(out, forward, up);
    return out;
}

function resizeCanvas()
{
    if (window.innerWidth > 1000)
        canvas.width = window.innerWidth * 0.75;
    else
        canvas.width = window.innerWidth * 0.8;
    canvas.height = canvas.width / aspectRatioCanvas;
}
