function degToRad(angle)
{
    return (angle * Math.PI / 180);
}

function LoadMesh(mesh)
{
    gl.bindVertexArray(mesh.vao);

    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuff);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mesh.vertices), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuff);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(mesh.indices), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuff);
    gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(mesh.colors), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuff);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mesh.normals), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, translationsBuffer);

    var translations = redoTranslationArray();
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(translations), gl.STATIC_DRAW);

    gl.bindVertexArray(null); //JMP: Not required but considered good practice

    return mesh;
}

function CreateMeshBuffers(mesh)
{
    //POSITIONS
    positionLoc = gl.getAttribLocation(currentProgram.program, "aPosition");
    gl.enableVertexAttribArray(positionLoc);
    vertexBuff = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuff);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mesh.vertices), gl.STATIC_DRAW);
    gl.vertexAttribPointer(positionLoc, 3, gl.FLOAT, false, 12, 0);

    //NORMALS
    normalLoc = gl.getAttribLocation(currentProgram.program, "aNormal");
    gl.enableVertexAttribArray(normalLoc);
    normalBuff = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, normalBuff);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(mesh.normals), gl.STATIC_DRAW);
    gl.vertexAttribPointer(normalLoc, 3, gl.FLOAT, false, 12, 0);

    //INDICES
    indexBuff = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuff);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(mesh.indices), gl.STATIC_DRAW);

    //COLORS
    colorLoc = gl.getAttribLocation(currentProgram.program, "aColor");
    gl.enableVertexAttribArray(colorLoc);

    colorBuff = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, colorBuff);
    gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(mesh.colors), gl.STATIC_DRAW);
    gl.vertexAttribPointer(colorLoc, 4, gl.UNSIGNED_BYTE, true, 4, 0);

    //Instancing
    var translations = redoTranslationArray();

    translationsLoc = gl.getAttribLocation(currentProgram.program, "aTranslation");
    gl.enableVertexAttribArray(translationsLoc);
    translationsBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, translationsBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(translations), gl.STATIC_DRAW);
    gl.vertexAttribPointer(translationsLoc, 3, gl.FLOAT, false, 12, 0);
    gl.vertexAttribDivisor(translationsLoc, 1); //1 == Update each new instance

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

}

function redoTranslationArray()
{
    var translations = [];
    var offset = instancingParameters.Offset;

    var halfX = Math.floor(instancingParameters.InstancesX * 0.5)
    var xRemainder = Math.ceil((instancingParameters.InstancesX * 0.5) % 1);

    var halfY = Math.floor(instancingParameters.InstancesY * 0.5)
    var yRemainder = Math.ceil((instancingParameters.InstancesY * 0.5) % 1);

    var halfZ = Math.floor(instancingParameters.InstancesZ * 0.5)
    var zRemainder = Math.ceil((instancingParameters.InstancesZ * 0.5) % 1);

    for (var x = -halfX; x < halfX + xRemainder; x++) {
        for (var y = -halfY; y < halfY + yRemainder; y++) {
            for (var z = -halfZ; z < halfZ + zRemainder; z++) {
                translations.push(x * offset);
                translations.push(y * offset);
                translations.push(z * offset);
            }
        }
    }
    return translations;
}

function UpdateUniforms()
{
    //Uniforms toon & Gooch
    switch (currentProgramIndex) {
        case idShaders.toon:
            gl.uniform1f(thicknessEdgeLocToon, shadedToonParameters.thicknessEdge);
            gl.uniform1f(levelsLocToon, shadedToonParameters.levels);
            break;

        case idShaders.gooch98:
            gl.uniform1f(thicknessEdgeLocGooch, shadedToonParameters.thicknessEdge);
            gl.uniform1f(levelsLocGooch, shadedToonParameters.levels);

            var backColor = glMatrix.vec3.fromValues(
                goochShaderParameters.backColor[0]/255,
                goochShaderParameters.backColor[1]/255,
                goochShaderParameters.backColor[2]/255);

            var frontColor = glMatrix.vec3.fromValues(
                goochShaderParameters.frontColor[0]/255,
                goochShaderParameters.frontColor[1]/255,
                goochShaderParameters.frontColor[2]/255);

            gl.uniform3fv(backColorGoochLoc, backColor);
            gl.uniform3fv(frontColorGoochLoc, frontColor);

            gl.uniform1f(backColorFacGoochLoc, goochShaderParameters.backColorFactor);
            gl.uniform1f(frontColorFacGoochLoc, goochShaderParameters.frontColorFactor);

            break;
    }

    //Matrices
    gl.bindBuffer(gl.UNIFORM_BUFFER, matricesUBO);

    //M
    gl.bufferSubData(gl.UNIFORM_BUFFER, 0, modelMat);

    //V
    gl.bufferSubData(gl.UNIFORM_BUFFER, 64, viewMat);

    //VP
    viewProjMat = glMatrix.mat4.create();
    glMatrix.mat4.multiply(viewProjMat, perspMat, viewMat);
    gl.bufferSubData(gl.UNIFORM_BUFFER, 128, viewProjMat);

    //N
    glMatrix.mat4.transpose(normalMat, glMatrix.mat4.invert(normalMat, modelViewMat));
    gl.bufferSubData(gl.UNIFORM_BUFFER, 192, normalMat);

    //Lights
    gl.bindBuffer(gl.UNIFORM_BUFFER, lightsUBO);
    var light0Pos = glMatrix.vec4.fromValues(LightParameters.positionX, LightParameters.positionY, LightParameters.positionZ, 1.0);

    var modelLightMat = glMatrix.mat4.create();
    glMatrix.mat4.identity(modelLightMat);
    var modelViewLightMat = glMatrix.mat4.create();
    glMatrix.mat4.multiply(modelViewLightMat, viewMat, modelLightMat);
    glMatrix.vec4.transformMat4(light0Pos, light0Pos, modelViewLightMat);

    gl.bufferSubData(gl.UNIFORM_BUFFER, 0, glMatrix.vec3.fromValues(light0Pos[0], light0Pos[1], light0Pos[2]));

    var light0Intensity = glMatrix.vec3.fromValues(LightParameters.intensity, LightParameters.intensity, LightParameters.intensity);
    gl.bufferSubData(gl.UNIFORM_BUFFER, 16, light0Intensity);


    gl.bindBuffer(gl.UNIFORM_BUFFER, null);
}

function InitializeUniforms()
{
    perspMat = glMatrix.mat4.create();
    glMatrix.mat4.perspective(perspMat, 45, canvas.width / canvas.height, 1, 1000);
    modelMat = glMatrix.mat4.create();
    glMatrix.mat4.identity(modelMat);
    viewMat = glMatrix.mat4.create();
    setViewMat(viewMat);
    modelViewProjMat = glMatrix.mat4.create();
    modelViewMat = glMatrix.mat4.create();
    normalMat = glMatrix.mat4.create();

    //Uniforms
    thicknessEdgeLocToon = gl.getUniformLocation(toonProgram.program, "uThicknessEdge");
    levelsLocToon = gl.getUniformLocation(toonProgram.program, "uLevels");

    thicknessEdgeLocGooch = gl.getUniformLocation(gooch98Program.program, "uThicknessEdge");
    levelsLocGooch = gl.getUniformLocation(gooch98Program.program, "uLevels");
    
    backColorGoochLoc = gl.getUniformLocation(gooch98Program.program, "ubackColor");
    backColorFacGoochLoc = gl.getUniformLocation(gooch98Program.program, "ubackColorFactor");
    frontColorGoochLoc = gl.getUniformLocation(gooch98Program.program, "ufrontColor");
    frontColorFacGoochLoc = gl.getUniformLocation(gooch98Program.program, "ufrontColorFactor");

    // UBOS
    phongMatricesUBOIndex = gl.getUniformBlockIndex(phongProgram.program, "Matrices");
    phongLightsUBOIndex = gl.getUniformBlockIndex(phongProgram.program, "LightInfo");
    gl.uniformBlockBinding(phongProgram.program, phongMatricesUBOIndex, 0);
    gl.uniformBlockBinding(phongProgram.program, phongLightsUBOIndex, 1);

    BPMatricesUBOIndex = gl.getUniformBlockIndex(blinnPhongProgram.program, "Matrices");
    BPLightsUBOIndex = gl.getUniformBlockIndex(blinnPhongProgram.program, "LightInfo");
    gl.uniformBlockBinding(blinnPhongProgram.program, BPMatricesUBOIndex, 0);
    gl.uniformBlockBinding(blinnPhongProgram.program, BPLightsUBOIndex, 1);

    toonMatricesUBOIndex = gl.getUniformBlockIndex(toonProgram.program, "Matrices");
    toonLightsUBOIndex = gl.getUniformBlockIndex(toonProgram.program, "LightInfo");
    gl.uniformBlockBinding(toonProgram.program, toonMatricesUBOIndex, 0);
    gl.uniformBlockBinding(toonProgram.program, toonLightsUBOIndex, 1);

    goochMatricesUBOIndex = gl.getUniformBlockIndex(gooch98Program.program, "Matrices");
    goochLightsUBOIndex = gl.getUniformBlockIndex(gooch98Program.program, "LightInfo");
    gl.uniformBlockBinding(gooch98Program.program, goochMatricesUBOIndex, 0);
    gl.uniformBlockBinding(gooch98Program.program, goochLightsUBOIndex, 1);

    //Matrices
    matricesUBO = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, matricesUBO);
    gl.bufferData(gl.UNIFORM_BUFFER, matricesUBOSize, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);
    gl.bindBufferRange(gl.UNIFORM_BUFFER, 0, matricesUBO, 0, matricesUBOSize);

    //Lights
    lightsUBO = gl.createBuffer();
    gl.bindBuffer(gl.UNIFORM_BUFFER, lightsUBO);
    gl.bufferData(gl.UNIFORM_BUFFER, lightsUBOSize, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.UNIFORM_BUFFER, null);
    gl.bindBufferRange(gl.UNIFORM_BUFFER, 1, lightsUBO, 0, lightsUBOSize);
}

function InitializePrograms()
{
    phongProgram = new ShaderManager();
    phongProgram.createShader(document.getElementById("vs").text, gl.VERTEX_SHADER);
    phongProgram.createShader(document.getElementById("fsPointPhongLighting").text, gl.FRAGMENT_SHADER);
    phongProgram.createProg();
    phongProgram.linkProg();

    blinnPhongProgram = new ShaderManager();
    blinnPhongProgram.createShader(document.getElementById("vs").text, gl.VERTEX_SHADER);
    blinnPhongProgram.createShader(document.getElementById("fsPointBlinnPhongLighting").text, gl.FRAGMENT_SHADER);
    blinnPhongProgram.createProg();
    blinnPhongProgram.linkProg();

    toonProgram = new ShaderManager();
    toonProgram.createShader(document.getElementById("vs").text, gl.VERTEX_SHADER);
    toonProgram.createShader(document.getElementById("fsToon").text, gl.FRAGMENT_SHADER);
    toonProgram.createProg();
    toonProgram.linkProg();


    gooch98Program = new ShaderManager();
    gooch98Program.createShader(document.getElementById("vs").text, gl.VERTEX_SHADER);
    gooch98Program.createShader(document.getElementById("Gooch98").text, gl.FRAGMENT_SHADER);
    gooch98Program.createProg();
    gooch98Program.linkProg();


    //First program in use
    gooch98Program.useProg();
    currentProgram = gooch98Program;
    currentProgramIndex = idShaders.gooch98;
}
