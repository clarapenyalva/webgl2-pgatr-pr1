//http://glmatrix.net/docs/module-mat4.html
var canvas;

//Program variables
var phongProgram, blinnPhongProgram, toonProgram, gooch98Program;
var currentProgram, currentProgramIndex;

//Attributes and uniforms locations
var projectionLoc, viewLoc, modelLoc, MVPMLoc, NMLoc;
var colorLoc, positionLoc, normalLoc;
var light0PosLoc, light0IntensityLoc;
var thicknessEdgeLocToon, levelsLocToon,
    thicknessEdgeLocGooch, levelsLocGooch,
    backColorGoochLoc, backColorFacGoochLoc,
    frontColorGoochLoc, frontColorFacGoochLoc;

//Matrices variables
var perspMat, viewMat, modelMat;
var modelViewProjMat, modelViewMat, normalMat, viewProjMat;

//Mesh buffers
var vertexBuff;
var indexBuff;
var normalBuff;
var colorBuff;

//UBOs
var lightsUBO, matricesUBO;

const matricesUBOSize = 256;
const lightsUBOSize = 60;

var phongMatricesUBOIndex;
var phongLightsUBOIndex;

var BPMatricesUBOIndex;
var BPLightsUBOIndex;

var toonMatricesUBOIndex;
var toonLightsUBOIndex;

var goochMatricesUBOIndex;
var goochLightsUBOIndex;

//Instancing
var instancingParameters =
{
    "InstancesX": 5,
    "InstancesY": 6,
    "InstancesZ": 5,
    "Offset":11
};

//Instancing translations array variables
var translationsBuffer, translationsLoc;

//Enums
var idModels = { cube: "0", knot: "1", bunny: "2", lion: "3" };
var idShaders = { phong: "0", blinnPhong: "1", toon: "2", gooch98: "3" }

//Structs
var LightParameters = {
    "intensity": 0.6,
    "positionX": 0.0,
    "positionY": 0.0,
    "positionZ": 2.0
};

var shadedToonParameters = {
    "thicknessEdge": 0.25,
    "levels": 6.0
};

var goochShaderParameters = {
    backColor: [ 20, 20, 140 ],
    backColorFactor: .25,
    frontColor: [ 140, 140, 20 ],
    frontColorFactor: .5
}

//Classes
class MeshData
{
    constructor(vertices, indices, normals, colors, vao, numInstances)
    {
        this.vertices = vertices;
        this.indices = indices;
        this.normals = normals;
        this.colors = colors;
        this.vao = vao;
        this.numInstances = numInstances;
    }
}

var currentMesh; //Of type MeshData

//user interface Dat.gui: https://github.com/dataarts/dat.gui 
window.onload = function ()
{
    var gui = new dat.GUI();

    var ChoiceModel = function ()
    {
        this.cube = function () { LoadModel(idModels.cube); };
        this.knot = function () { LoadModel(idModels.knot); };
        this.bunny = function () { LoadModel(idModels.bunny); };
        this.lion = function () { LoadModel(idModels.lion); };
    };
    choiceModel = new ChoiceModel();
    var folderGeneral = gui.addFolder('ModelSelection');
    folderGeneral.add(choiceModel, "cube");
    folderGeneral.add(choiceModel, "knot");
    folderGeneral.add(choiceModel, "bunny");
    folderGeneral.add(choiceModel, "lion");
    folderGeneral.close();

    var ChoiceShader = function ()
    {
        this.phong = function () { LoadShader(idShaders.phong); };
        this.blinnPhong = function () { LoadShader(idShaders.blinnPhong); };
        this.toon = function () { LoadShader(idShaders.toon); };
        this.gooch98 = function () { LoadShader(idShaders.gooch98); };
    };
    choiceShader = new ChoiceShader();
    var folderShaders = gui.addFolder('Shaders');
    folderShaders.add(choiceShader, "phong");
    folderShaders.add(choiceShader, "blinnPhong");
    folderShaders.add(choiceShader, "toon");
    folderShaders.add(choiceShader, "gooch98");

    var folderLight = gui.addFolder('LightParameters');
    folderLight.add(LightParameters, 'intensity', 0, 1);
    folderLight.add(LightParameters, 'positionX', -100, 100);
    folderLight.add(LightParameters, 'positionY', -100, 100);
    folderLight.add(LightParameters, 'positionZ', -100, 100);
    folderLight.close();

    var folderShaderPars = gui.addFolder('Shader Parameters');

    var folderToon = folderShaderPars.addFolder('Toon Parameters');
    folderToon.add(shadedToonParameters, 'thicknessEdge', 0, 1);
    folderToon.add(shadedToonParameters, 'levels', 1, 50).step(1);
    folderToon.close();

    var folderGooch = folderShaderPars.addFolder('Gooch Parameters');
    folderGooch.addColor(goochShaderParameters, 'backColor');
    folderGooch.add(goochShaderParameters, 'backColorFactor', 0, 1);
    folderGooch.addColor(goochShaderParameters, 'frontColor');
    folderGooch.add(goochShaderParameters, 'frontColorFactor', 0, 1);

    var folderInstancing = gui.addFolder('Instancing');
    folderInstancing.add(instancingParameters, 'InstancesX', 1, 20).step(1).onChange(ReloadModel);
    folderInstancing.add(instancingParameters, 'InstancesY', 1, 20).step(1).onChange(ReloadModel);
    folderInstancing.add(instancingParameters, 'InstancesZ', 1, 20).step(1).onChange(ReloadModel);
    folderInstancing.add(instancingParameters, 'Offset', 1, 30).onChange(ReloadModel);
};

//Reloads the same model with different instancing values
function ReloadModel()
{
    instancingParameters.InstancesX = Math.floor(instancingParameters.InstancesX);
    instancingParameters.InstancesY = Math.floor(instancingParameters.InstancesY);
    instancingParameters.InstancesZ = Math.floor(instancingParameters.InstancesZ);

    currentMesh.numInstances = glMatrix.vec3.fromValues(
        instancingParameters.InstancesX,
        instancingParameters.InstancesY,
        instancingParameters.InstancesZ);

    LoadMesh(currentMesh);
}

function LoadShader(idShader)
{
    switch (idShader) {
        case idShaders.phong:
            phongProgram.useProg();
            currentProgram = phongProgram;
            break;

        case idShaders.blinnPhong:
            blinnPhongProgram.useProg();
            currentProgram = blinnPhongProgram;
            break;

        case idShaders.toon:
            toonProgram.useProg();
            currentProgram = toonProgram;
            break;

        case idShaders.gooch98:
            gooch98Program.useProg();
            currentProgram = gooch98Program;
            break;
    }

    currentProgramIndex = idShader;
}

//Loads a model chosen from the UI.
function LoadModel(idModel)
{
    glMatrix.mat4.identity(modelMat);
    switch (idModel) {
        case idModels.cube:
            mesh = new MeshData(cube.vertices[0].values,
                cube.connectivity[0].indices,
                cube.vertices[1].values,
                cube.vertices[2].values,
                currentMesh.vao,
                glMatrix.vec3.fromValues(
                    instancingParameters.InstancesX,
                    instancingParameters.InstancesY,
                    instancingParameters.InstancesZ)
            );

            glMatrix.mat4.translate(modelMat, modelMat, glMatrix.vec3.fromValues(0, -.5, 0));
            break;

        case idModels.knot:
            mesh = new MeshData(knot.vertices[0].values,
                knot.connectivity[0].indices,
                knot.vertices[1].values,
                knot.vertices[2].values,
                currentMesh.vao,
                glMatrix.vec3.fromValues(
                    instancingParameters.InstancesX,
                    instancingParameters.InstancesY,
                    instancingParameters.InstancesZ)
            );
            break;

        case idModels.bunny:
            mesh = new MeshData(bunny.vertices[0].values,
                bunny.connectivity[0].indices,
                bunny.vertices[1].values,
                bunny.vertices[2].values,
                currentMesh.vao,
                glMatrix.vec3.fromValues(
                    instancingParameters.InstancesX,
                    instancingParameters.InstancesY,
                    instancingParameters.InstancesZ)
            );
            glMatrix.mat4.scale(modelMat, modelMat, glMatrix.vec3.fromValues(.1, .1, .1)); //scale for the bunny
            glMatrix.mat4.translate(modelMat, modelMat, glMatrix.vec3.fromValues(0, -10, 0));
            break;

        case idModels.lion:
            mesh = new MeshData(lion.vertices[0].values,
                lion.connectivity[0].indices,
                lion.vertices[1].values,
                lion.vertices[2].values,
                currentMesh.vao,
                glMatrix.vec3.fromValues(
                    instancingParameters.InstancesX,
                    instancingParameters.InstancesY,
                    instancingParameters.InstancesZ)
            );
            glMatrix.mat4.scale(modelMat, modelMat, glMatrix.vec3.fromValues(1.5, 1.5, 1.5)); //scale for the lion
            glMatrix.mat4.translate(modelMat, modelMat, glMatrix.vec3.fromValues(42, -.5, -7.5));
            break;
    }

    currentMesh = LoadMesh(mesh);
}

//Init function
function Init()
{
    canvas = document.getElementById("glCanvas");
    //Size canvas
    aspectRatioCanvas = 16.0 / 9.0;
    if (window.innerWidth > 1000)
        canvas.width = window.innerWidth * 0.6;
    else
        canvas.width = window.innerWidth * 0.8;
    canvas.height = canvas.width / aspectRatioCanvas;

    canvas.oncontextmenu = function (e)
    {
        e.preventDefault();
    };
    //Context Inicialization
    try {
        gl = canvas.getContext("webgl2");
    } catch (e) {
        alert("Your browser is not webgl2 compatible!!!");
        throw "Unable to initialize WebGL2 context";
    }

    window.addEventListener('resize', resizeCanvas, false);

    InitializeControls(canvas);

    //Shader inicialization
    InitializePrograms();

    //Attach Matrices
    InitializeUniforms();

    //Cube VAO initialization
    var vao = gl.createVertexArray();
    gl.bindVertexArray(vao);
    currentMesh = new MeshData(knot.vertices[0].values,
        knot.connectivity[0].indices,
        knot.vertices[1].values,
        knot.vertices[2].values,
        vao,
        glMatrix.vec3.fromValues(
            instancingParameters.InstancesX,
            instancingParameters.InstancesY,
            instancingParameters.InstancesZ)
    );
    CreateMeshBuffers(currentMesh);
    gl.bindVertexArray(null); 

    //Prepare render
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    gl.clearDepth(1.0);
}

//Draw funtion
function Draw(time)
{
    if (!drag && !move) dX = dY = 0;

    //Rendering
    gl.viewport(0.0, 0.0, canvas.width, canvas.height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    UpdateUniforms();

    gl.bindVertexArray(currentMesh.vao);

    var instancesNum = currentMesh.numInstances[0] * currentMesh.numInstances[1] * currentMesh.numInstances[2];
    gl.drawElementsInstanced(gl.TRIANGLES, currentMesh.indices.length, gl.UNSIGNED_SHORT, 0, instancesNum);
    gl.bindVertexArray(null); 

    gl.flush();
    window.requestAnimationFrame(Draw);
};

Init();
Draw(0);
