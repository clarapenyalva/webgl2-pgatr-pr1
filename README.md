# WebGL 2.0 [PGATR Pr1]

First practice assignment from Graphics Processors and Real-Time Applications. 
Master's Degree in Computer Graphics, Rey Juan Carlos University (Madrid).

Developed with WebGL API, HTML5, CSS3 and JavaSript and the high-level library dat.GUI to add a user interface.

Tasks performed using the API provided by WebGL 2.0:
- Blinn-Phong lighting model.
- NPR technique based on Toon Shading.
- NPR technique based on gooch98 paper «A Non-Photorealistic Lighting Model For Automatic Technical Illustration» (Gooch, Gooch, Shirley, & Cohen, 1998).
- Advanced WebGL2: VAOs, UBOs and VBOs to facilitate dynamic changing of shaders and instantiation of objects.

[Developed in 2019]

## Credits
*  Clara Peñalva Carbonell  [[webpage](https://clarapenyalva.com)] [[GitLab profile](https://gitlab.com/clarapenyalva)] 
*  José María Pizana García [[webpage](https://jmpizana.com)] [[GitLab profile](https://gitlab.com/jmpizanagarcia)]

---
Project icon by DELAPOUITE: https://delapouite.com/
Project background image by rawpixel.com / Freepik: http://www.freepik.com
